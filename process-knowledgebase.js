const fs = require('fs');
const ss = require('simple-statistics');

// look for a file arg in the first param
// if not, use all.json
let inFile = process.argv[2] || 'knowledgebase.json';

let kb = JSON.parse(fs.readFileSync(inFile));
let prob = JSON.parse(fs.readFileSync('predictionFrequencies.json'));

//TODO: calculate
let falsePos = new Array(10).fill(0);
let falseNeg = new Array(10).fill(0);

/**
 * Compose storage for accounting 
 *     preda            predb
 *     [model a] [0-9] [model b] [model_pred 0 - 9] [awrong, bwrong]
 *       
 */

probas = [];
// one object for each digit plus an array of 10 counts
let falseErrors = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]];
for (let i = 0; i < 10; i++) {
    let prob = {};
    for (let key in kb[0].probabilityPredictions) {
        let model = {};
        model['values'] = [];
        prob[key] = model;
        for (let j = 0; j < 10; j++) {
            model['values'].push([]);
        }
        falseErrors[i][key] = {falsePositives: 0, falseNegatives: 0};
    }
    //console.log(JSON.stringify(prob, null, 4));
    probas.push(prob);
}

let count = 0;
for (let pred of kb) {
    falseErrors[10][pred.label]++;
    let digitScores = new Array(10).fill(0);
    let descs = [[],[],[],[],[],[],[],[],[],[]];
    for (let model in pred.predictions) {
        let found = true;
        if (!pred.predictions[model].includes(pred.label)) {
            falseErrors[pred.label][model]['falseNegatives']++;
            found = false;
        }
        for (let prediction of pred.predictions[model]) {
            let p = prob[model + "Prob"][prediction];
            digitScores[prediction] += p;
            descs[prediction].push(model);
            if (!found) {
                falseErrors[prediction][model]['falsePositives']++;
            }
        }
    }
    let tot = digitScores.reduce((a, b) => {
        return a + b;
    }, 0);
    let probs = [];
    for (let i = 0; i < digitScores.length; i++) {
        let digitScore = digitScores[i];
        if (digitScore > 0) {
            let p = {prediction: i, prob: digitScore/tot, desc: descs[i]};
            probs.push(p);
        }
    }
    probs.sort((a, b) => {
        if (a.prob < b.prob) {
            return 1;
        } else if (a.prob > b.prob) {
            return -1;
        }
        return 0;
    });
    pred['probs'] = probs;

    // add the the explainable rationale
    let ix = 0;
    for (let p of probs) {
        let rationale = `The character ${p.prediction} `
        if (ix == 0) {
            rationale += 'was selected with ';
        } else {
            rationale += 'was not selected because it only had '
        }
        rationale += `${p.prob.toFixed(2)} effective confidence due to consistency with the ${p.desc} properties`;

        p['explanation'] = rationale;
        ix++
    }
    pred['probs'] = probs;

    /*
    if (probs[0] && probs[0].prediction != pred.label) {
        falseErrors[pred.label].falseNegatives++;
        if (probs.length > 0) {
            falseErrors[probs[0].prediction].falsePositives++;
        }
    }
    */

    // process probabilityPredictions
    //console.log(pred.probabilityPredictions);
    for (let key in pred.probabilityPredictions) {
        let index = 0;
        //console.log(key, pred.probabilityPredictions[key].length)
        for (let prob of pred.probabilityPredictions[key]) {
            //console.log('label:', pred.label, probas);
            probas[pred.label][key]['values'][index].push(prob);
            index++;
        }
    }

    count++;
}

for (let label of probas) {
    //console.log(label)
    for(let model in label) {
        let means = [];
        //console.log(model, label[model])
        for (let digit of label[model]['values']) {
            if (digit.length > 0) {
                let mean = ss.mean(digit);
                means.push(mean);
            }
        }
        if (means.length > 0) {
            label[model]['means'] = means;
            label[model]['stdDev'] = ss.standardDeviation(means);
            label[model]['kurtosis'] = ss.sampleKurtosis(means);
            label[model]['skew'] = ss.sampleSkewness(means);
        }
    }
}


console.log('count:', count);
let correct = 0;
count = 0
let incorrects = new Array(10).fill(0); // track which digits are incorrect
let incorrectNames = [];
for (let pred of kb) {
    if (pred.probs[0] && pred.label == pred.probs[0].prediction) {
        correct++;
    } else {
        incorrects[pred.label]++;
        incorrectNames.push(pred.name);
    }
    count++;
}

console.log('correct:', correct, 'count:', count, 'prediction accuracy:', correct/count);
console.log('incorrects per digit:', incorrects);

fs.writeFileSync('knowledgebase-pred.json', JSON.stringify(kb, null, 4));
fs.writeFileSync('knowledgebase-incorrect-names.json', JSON.stringify(incorrectNames, null, 4));
fs.writeFileSync('knowledgebase-probs.json', JSON.stringify(probas, null, 4));

for (let label of probas) {
    for(let model in label) {
        label[model].values = undefined;
    }
    
}

fs.writeFileSync('knowledgebase-probs-summary.json', JSON.stringify(probas, null, 4));
fs.writeFileSync('false-errors.json', JSON.stringify(falseErrors, null, 4));