#!/bin/bash

mkdir -p ../data-py/img
mkdir -p ../data-py/skel-lee
mkdir -p ../data-py/skel-zhang
mkdir -p ../data-py/fill-skel
mkdir -p ../data-py/fill
mkdir -p ../data-py/cross-end
mkdir -p ../data-py/dil
mkdir -p ../data-py/dil2
mkdir -p ../data-py/crossing
mkdir -p ../data-py/endpoint
mkdir -p ../data-py/dil-skel
mkdir -p ../data-py/dil2-skel
mkdir -p ../data-py/dil-fill
mkdir -p ../data-py/dil2-fill
mkdir -p ../data-py/line
mkdir -p ../data-py/ellipse
mkdir -p ../data-py/circle
mkdir -p ../data-py/ellipse_circle
mkdir -p ../data-py/raw
mkdir -p ../data-py/opening
mkdir -p ../data-py/fft
mkdir -p ../data-py/chull

