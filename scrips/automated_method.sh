#!/bin/bash

# # transform
# mkdir -p transforms
# python mnist-skeleton.py -i ~/mnist/download/train-images-idx3-ubyte -l ~/mnist/download/train-labels-idx1-ubyte -o ./transforms/
# mkdir -p test-transforms
# python mnist-skeleton.py -i ~/mnist/download/t10k-images-idx3-ubyte -l ~/mnist/download/t10k-labels-idx1-ubyte -o ./test-transforms/

# #train
# mkdir -p models
# python mnist_train_multiple.py

#process
python mnist_run_all_models_test.py # result is all.json

node process-predictions.js

node read-all.js

node process-knowledgebase.js

