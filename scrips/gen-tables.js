const fs = require('fs');
const path = require('path');

let pf = JSON.parse(fs.readFileSync('predictionFrequencies.json'));
/*
 "skeletonProb": [
        1,
        1,
        1,
        '''
 ],
"count": 60000,
"modelNames": [
    "skeleton",
    "circle",
    "crossing",
    "ellipse",
    "ellipseCircle",
    "endpoint",
    "fill",
    "line",
    "skeletonFill",
    "baseLine"
]
    */

let ps = JSON.parse(fs.readFileSync('knowledgebase-probs-summary.json'));

/*
    {
        "skeleton": {
            "means": [
                0.9998622745845012,
                9.285541046877248e-7,
                0.00001750483619657079,
                0.000010218760074958145,
                0.000007701922238678877,
                0.000008396220793818228,
                0.000021181469328235447,
                0.0000035818593523868487,
                0.000010490326221617986,
                0.000015687654414738352
            ],
            "stdDev": 0.29995549271222177,
            "kurtosis": 9.999999989555363,
            "skew": 3.1622776579164147
        },
*/

let fe = JSON.parse(fs.readFileSync('false-errors.json'));

/*
{
        "skeleton": {
            "falsePositives": 0,
            "falseNegatives": 0
        },
        "circle": {
            "falsePositives": 846,
            "falseNegatives": 2966
        },
*/

// used to replce the table header digit
function replaceAt(str, index, replacement) {
    return str.substr(0, index) + replacement + str.substr(index + replacement.toString().length);
}

// set up the columns
let properties = [];
for (let property in ps[0]) {
    properties.push(property);
}
const NUM_COLUMNS = properties.length;
// set column layout
let tableLayout = ' | c || ';
let tableHeader = ' digit 0';
let propertyIds = [];
for (let i = 0; i < NUM_COLUMNS; i++) {
    tableLayout += ' c |';
    tableHeader += ` & $P_${i+1}$`;
    propertyIds.push(`$P_${i+1}$`);
}
tableHeader += ' \\\\';

// output the property ids
let propIdTable = `\n\nProperties\n\n\\begin{tabular}{| c | c |}\n Property Name & Property Id \\\\\n\\hline\\hline\n`;
for (let i = 0; i < properties.length; i++) {
    propIdTable += `${properties[i]} & ${propertyIds[i]} \\\\\n\\hline\n`
}
propIdTable += '\\end{tabular}\n';

console.log(propIdTable);

let totalCount = 0;
for (let c of fe[10]) {
    totalCount += c;
}

for (let digit = 0; digit < 10; digit++) {
    let totalFalse = totalCount - fe[10][digit];
    let pctCorr = 'likelihood $L_{\\pi,5}$ ';
    let stdDev = '$\\sigma$ ';
    let kurtosis = 'k ';
    let skew = 'skew ';
    let falsePos = 'false positive ';
    let falseNeg = 'false negative ';
    let digitLines = []; let digitMeans = [];

    for (let prop of properties) {
        pctCorr += ' & ' + (pf[prop + 'Prob'][digit]* 100).toFixed(1);
        falsePos += ' & ' + (100 * fe[digit][prop].falsePositives / totalFalse).toFixed(1);
        //$\\frac{' + fe[digit][prop].falsePositives + '}{' + totalFalse + '}$
        falseNeg += ' & ' + (100 * fe[digit][prop].falseNegatives / fe[10][digit]).toFixed(1);
        //$\\frac{' + fe[digit][prop].falseNegatives + '}{'  + fe[10][digit] + '}$
    }

    for (let i = 0; i < 10; i++) {
        digitMeans.push([]);
        digitLines.push(i.toString())
    }
    for (let key in ps[digit]) {
        stdDev += '& ' + ps[digit][key].stdDev.toFixed(3);
        kurtosis += '& ' + ps[digit][key].kurtosis.toFixed(3);
        skew += '& ' + ps[digit][key].skew.toFixed(3);
        let index = 0;
        for (let mean of ps[digit][key].means) {
            digitLines[index] += ' & ' + mean.toFixed(3)
            digitMeans[index].push(mean);
            index++
        }
    }

    let output = `\\begin{table}\n\\caption{Digit ${digit} Outputs}\n\\centering\\begin{tabular}{${tableLayout}}\n`;
    output += replaceAt(tableHeader, 7, digit) + '\n';
    output += '\\hline \\hline\n';
    output += pctCorr + ' \\\\\n';
    output += '\\hline\n';
    output += stdDev + ' \\\\\n';
    output += '\\hline\n';
    output += kurtosis + ' \\\\\n';
    output += '\\hline\n';
    output += skew + ' \\\\\n';
    output += '\\hline\n';
    for (let dl of digitLines) {
        output += dl + ' \\\\\n';
        output += '\\hline\n';
    }
    output += falsePos + ' \\\\\n';
    output += '\\hline\n';
    output += falseNeg + ' \\\\\n';
    output += '\\hline\n';
    output += '\\end{tabular}\n\\end{table}\n\n';
    console.log(output);

    fs.writeFileSync(path.join('mean-plot', `${digit}-mean-digit.json`), JSON.stringify(digitMeans, null, 4));

}
