#!/bin/bash

python mnist_predict.py -m mnist_circle.model -i ~/mnist/train-transform/circle-image -l ~/mnist/train-transform/circle-labels -t circle -o ~/tmp

python mnist_predict.py -m mnist_crossing.model -i ~/mnist/train-transform/crossing-image -l ~/mnist/train-transform/crossing-labels -t crossing -o ~/tmp

python mnist_predict.py -m mnist_ellipse.model -i ~/mnist/train-transform/ellipse-image -l ~/mnist/train-transform/ellipse-labels -t ellipse -o ~/tmp

python mnist_predict.py -m mnist_ellipse_circle.model -i ~/mnist/train-transform/ellipse_circle-image -l ~/mnist/train-transform/ellipse_circle-labels -t ellipse_circle -o ~/tmp

python mnist_predict.py -m mnist_endpoint.model -i ~/mnist/train-transform/endpoint-image -l ~/mnist/train-transform/endpoint-labels -t endpoint -o ~/tmp

python mnist_predict.py -m mnist_fill.model -i ~/mnist/train-transform/fill-image -l ~/mnist/train-transform/fill-labels -t fill -o ~/tmp

python mnist_predict.py -m mnist_line.model -i ~/mnist/train-transform/line-image -l ~/mnist/train-transform/line-labels -t line -o ~/tmp

python mnist_predict.py -m mnist_skel_fill.model -i ~/mnist/train-transform/skel-fill-image -l ~/mnist/train-transform/skel-fill-labels -t skel_fill -o ~/tmp

python mnist_predict.py -m mnist_skeleton.model -i ~/mnist/train-transform/skel-image -l ~/mnist/train-transform/skel-labels -t skeleton -o ~/tmp

python mnist_predict.py -m mnist_raw.model -i ~/mnist/download/train-images-idx3-ubyte -l ~/mnist/download/train-labels-idx1-ubyte -t raw -o ~/tmp

#============================================
# test figs
#============================================

#baseline test
python mnist_baseline_test_fig.py -i ~/mnist/download/t10k-images-idx3-ubyte -l ~/mnist/download/t10k-labels-idx1-ubyte -t test_baseline -o ~/tmp