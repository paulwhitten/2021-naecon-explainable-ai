const fs = require('fs');

/**
 * builds the knowledgebase
 */

function getFrequencyArray(qty) {
    return Array(qty).fill(0);
}

/** creates an object to store the frequency of predictions
 * will contain a field for each property description
 * containing an array of elements one for each digit
 * plus an incorrect
*/
function createPredictionFrequencies(propertyDescriptions) {
    let result = {};
    for (let desc of propertyDescriptions) {
        result[desc] = getFrequencyArray(11);
    }
    result['baseLine'] = new Array(10).fill(0);
    return result;
}

function checkPrediction(label, predictions, predictionFrequencies) {
    // enumerate over property keys
    for (let key in predictions) {
        if (predictions[key].length == 1) {
            if (predictions[key][0] == label) {
                predictionFrequencies[key][label]++;
            } else {
                predictionFrequencies[key][10]++; //incorrect
            }
        } else {
            // TODO: handle multiple predictions
            predictionFrequencies[key][10]++; //incorrect
        }
    }
    predictionFrequencies.baseLine[label]++;
}

// look for a file arg in the first param
// if not, use all.json
let inFile = process.argv[2] || 'all-with-names.json';

let baselineFrequency = getFrequencyArray(10);

let data = JSON.parse(fs.readFileSync(inFile));
/* fields
console.log('predictions:', data['predictions'].length);
console.log('labels:', data['labels'].length);
console.log('propertyDescriptions', data['propertyDescriptions'].length);
console.log('propertyScores', data['propertyScores'].length);
// names
*/

let predictionFrequencies = createPredictionFrequencies(data.propertyDescriptions);

function getPrediction(prediction) {
    let result = [];
    let index = 0;
    for (let pred of prediction) {
        if (pred != 0) {
            result.push(index);
        }
        index++;
    }
    return result;
}

function predictionsToObject(predictions, propertyDescriptions) {
    let result = {};
    let index = 0;
    for (let desc of propertyDescriptions) {
        result[desc] = getPrediction(predictions[index]);
        index++;
    }
    return result;
}

function probsToObject(probs, propertyDescriptions) {
    let result = {};
    let index = 0;
    for (let desc of propertyDescriptions) {
        //console.log(propertyDescriptions.length, probs.length)
        result[desc] = probs[index];
        index++;
    }
    return result;
}

let index = 0;
knowledgebase = [];
// loop on all predictions
while (index < data.names.length) {
    let label = data.labels[index];
    let image = {
        index: index,
        name: data.names[index],
        label: label,
        predictions: predictionsToObject(data.predictions[index], data.propertyDescriptions),
        probabilityPredictions: probsToObject(data.probabilitiyPredictions[index], data.propertyDescriptions)
    }
    checkPrediction(label, image.predictions, predictionFrequencies);
    knowledgebase.push(image);
    baselineFrequency[label]++;

    index++;
}

console.log('counted', index, "predictions");

let modelN = [];
for (let freq in predictionFrequencies) {
    modelN.push(freq);
    let prob = [];
    for (let i = 0; i < 10; i++) {
        prob.push(predictionFrequencies[freq][i] / predictionFrequencies['baseLine'][i])
    }
    predictionFrequencies[freq + 'Prob'] = prob;
}

predictionFrequencies['count'] = index;
predictionFrequencies['modelNames'] = modelN;

let outFile = 'knowledgebase.json';
if (process.argv.length > 2) {
    outFile = 'knowledgebase.test.json';
}

fs.writeFileSync(outFile, JSON.stringify(knowledgebase, null, 4));

if (!outFile.endsWith('test.json')) {
    fs.writeFileSync('predictionFrequencies.json', JSON.stringify(predictionFrequencies, null, 4));
    fs.writeFileSync('probPredictions.json', JSON.stringify(predictionFrequencies, null, 4));
}
