const fs = require('fs');

/**
 * Reads all predictions stored in json
 * adds useful names (like images) to predictions
 * 
output_json['predictions'] = results
output_json['probabilitiyPredictions'] = proba_results
output_json['labels'] = int_labels
output_json['propertyDescriptions'] = property_descriptions
output_json['propertyScores'] = scores
*/

// look for a file arg in the first param
// if not, use all.json
let inFile = process.argv[2] || 'all.json';
// pick the appropriate name for the output file
let outFile = inFile.replace(".", "-with-names.");

let results = JSON.parse(fs.readFileSync(inFile));
let counts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

console.log('label length:', results.labels.length);
let names = [];
let count = 0;
for (let label of results.labels) {
    let name = label + '-' + counts[label]++;
    names.push(name);
    //console.log(name);
    count++;
}
console.log('created names:', count);

results['names'] = names;

fs.writeFileSync(outFile, JSON.stringify(results, null, 4));