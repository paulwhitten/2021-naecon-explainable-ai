# 2021 naecon explainable xai

This folder contains implementation of the work for
[Explainable Artificial Intelligence Methodology for Handwritten Applications](https://doi.org/10.1109/NAECON49338.2021.9696413)
The paper source can be found [here](https://github.com/paulwhitten/2021-naecon-xai_methodology_for_handwritten_applications).

The file [method.md](method.md) explains the sequence of steps to run scripts to produce results.
