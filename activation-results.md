
The following table depicts the performance of the following a activation functions for maching handwritten digits.

| Image Type        | Rectified Linear |   tanh      |  logistic   |    Notes  |
| :-------          | :-----:          |  :-----:    |  :-----:    | :-------- |
| original          |  **0.977**       |  0.970      |   0.966     |           |
| skeleton          |  **0.950**       |  0.949      |  0.943      |           |
| endpoints         | 0.797            |  0.792      |  **0.800**  |           |
| **crossing**      |  0.163           |  **0.203**  |  0.182      |           |
| fill              | 0.346            |  **0.348**  |  0.345      |           |
| skeleton fill     | 0.360            |  **0.364**  |  0.360      |           |
| line              | **0.500**        |  0.480      | 0.492       |           |
| ellipse           | 0.153            |  **0.155**  | 0.151       |           |
| circle            | 0.293            |  0.188      | **0.298**   |           |
| ellipse or circle | 0.339            |  0.237      | **0.352**   | modified ellipse to return ellipse or multiple circles |
