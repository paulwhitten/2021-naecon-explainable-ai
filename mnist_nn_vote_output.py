import pickle
import json
import numpy as np
from sklearn.neural_network import MLPClassifier
import pickle


with open('all.training.json') as infile:
    allData = json.load(infile)

count = 0
input_data = []
for preds in allData['predictions']:
    #if count == 0:
        #print(preds)
        #print(numpy.concatenate(preds))
    flat_list = np.concatenate(preds)
    flat_floats = np.zeros(len(flat_list), np.float)
    for i in range(len(flat_list)):
        if flat_list[i] != 0:
            flat_floats[i] = float(flat_list[i])
    input_data.append(flat_floats)
    #print(flat_floats)
    count = count + 1

search_digit = 2
search_count = 4
output_data = []
label_count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
i = 0
found = 0
for label in allData['labels']:
    if (label == search_digit and search_count == label_count[label]):
        found = i
    row = np.zeros(10, np.float)
    row[label] = 1.0
    output_data.append(row)
    label_count[label] = label_count[label] + 1
    i = i + 1
    #print(row)

print(count, i)

nn = pickle.load(open('mnist_nn_vote.model', 'rb'))

result = nn.predict_proba([input_data[found]])

print(result)