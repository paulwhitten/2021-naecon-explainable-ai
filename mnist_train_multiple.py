import argparse
from load_mnist_data import load_mnist_float
from sklearn.neural_network import MLPClassifier
import pickle

parser = argparse.ArgumentParser(description='Trains the models')
parser.add_argument('-m', '--mnist_folder', 
                        help='The mnist folder')
parser.add_argument('-t', '--test_transform_folder', 
                        help='The folder where training transforms are stored')
parser.add_argument('-r', '--train_transform_folder', 
                        help='The folder where test transforms are stored')
args = parser.parse_args()

# mlp hidden layers
hidden_layers = (100, 200)

print('hidden layers:', hidden_layers)

# # original
# print("======Loading original training data.======")
# N, rows, columns, training_digits, training_labels = load_mnist_float(
#     args.mnist_folder + "/train-images-idx3-ubyte", args.mnist_folder + "/train-labels-idx1-ubyte")
# print("loaded", N)
# mlp = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='relu', max_iter=500, alpha=1e-4,
#                     solver='sgd', verbose=10, random_state=1,
#                     learning_rate_init=.1)

# mlp.fit(training_digits, training_labels)
# print("Training set score: %f" % mlp.score(training_digits, training_labels))

# print("Loading test data.")
# n, rows, columns, test_digits, test_labels = load_mnist_float(
#     args.mnist_folder + "/t10k-images-idx3-ubyte", args.mnist_folder + "/t10k-labels-idx1-ubyte")
# print("loaded", n)
# print("Test set score: %f" % mlp.score(test_digits, test_labels))
# pickle.dump(mlp, open('./models/mnist_raw.model', 'wb'))

# # skeleton
# print('')
# print('')
# print("======Loading skeleton training data.======")
# N, rows, columns, training_digits, training_labels = load_mnist_float(
#     args.train_transform_folder + "/skel-image", args.train_transform_folder + "/skel-labels")
# print("loaded", N)
# mlp1 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='relu', max_iter=500, alpha=1e-4,
#                     solver='sgd', verbose=10, random_state=1,
#                     learning_rate_init=.1)

# mlp1.fit(training_digits, training_labels)
# print("Training set score: %f" % mlp1.score(training_digits, training_labels))

# print("Loading test data.")
# n, rows, columns, test_digits, test_labels = load_mnist_float(
#     args.test_transform_folder + "/skel-image", args.test_transform_folder + "/skel-labels")
# print("loaded", n)
# print("Test set score: %f" % mlp1.score(test_digits, test_labels))
# pickle.dump(mlp1, open('./models/mnist_skeleton.model', 'wb'))

# # endpoints
# print('')
# print('')
# print("======Loading endpoints training data.======")
# N, rows, columns, training_digits, training_labels = load_mnist_float(
#     args.train_transform_folder + "/endpoint-image", args.train_transform_folder + "/endpoint-labels")
# print("loaded", N)
# mlp2 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='logistic', max_iter=500, alpha=1e-4,
#                     solver='sgd', verbose=10, random_state=1,
#                     learning_rate_init=.1)

# mlp2.fit(training_digits, training_labels)
# print("Training set score: %f" % mlp2.score(training_digits, training_labels))

# print("Loading test data.")
# n, rows, columns, test_digits, test_labels = load_mnist_float(
#     args.test_transform_folder + "/endpoint-image", args.test_transform_folder + "/endpoint-labels")
# print("loaded", n)
# print("Test set score: %f" % mlp2.score(test_digits, test_labels))
# pickle.dump(mlp2, open('./models/mnist_endpoint.model', 'wb'))

# crossings
print('')
print('')
print("======Loading crossings training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    args.train_transform_folder + "/crossing-image", args.train_transform_folder + "/crossing-labels")
print("loaded", N)
mlp3 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='tanh', max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp3.fit(training_digits, training_labels)
print("Training set score: %f" % mlp3.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    args.test_transform_folder + "/crossing-image", args.test_transform_folder + "/crossing-labels")
print("loaded", n)
print("Test set score: %f" % mlp3.score(test_digits, test_labels))
pickle.dump(mlp3, open('./models/mnist_crossing.model', 'wb'))

# fill
print('')
print('')
print("======Loading fill training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    args.train_transform_folder + "/fill-image", args.train_transform_folder + "/fill-labels")
print("loaded", N)
mlp4 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='tanh', max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp4.fit(training_digits, training_labels)
print("Training set score: %f" % mlp4.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    args.test_transform_folder + "/fill-image", args.test_transform_folder + "/fill-labels")
print("loaded", n)
print("Test set score: %f" % mlp4.score(test_digits, test_labels))
pickle.dump(mlp4, open('./models/mnist_fill.model', 'wb'))

# skel fill
print('')
print('')
print("======Loading skel fill training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    args.train_transform_folder + "/skel-fill-image", args.train_transform_folder + "/skel-fill-labels")
print("loaded", N)
mlp5 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='tanh', max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp5.fit(training_digits, training_labels)
print("Training set score: %f" % mlp5.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    args.test_transform_folder + "/skel-fill-image", args.test_transform_folder + "/skel-fill-labels")
print("loaded", n)
print("Test set score: %f" % mlp5.score(test_digits, test_labels))
pickle.dump(mlp5, open('./models/mnist_skel_fill.model', 'wb'))

# lines
print('')
print('')
print("======Loading line training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    args.train_transform_folder + "/line-image", args.train_transform_folder + "/line-labels")
print("loaded", N)
mlp6 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='relu', max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp6.fit(training_digits, training_labels)
print("Training set score: %f" % mlp6.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    args.test_transform_folder + "/line-image", args.test_transform_folder + "/line-labels")
print("loaded", n)
print("Test set score: %f" % mlp6.score(test_digits, test_labels))
pickle.dump(mlp6, open('./models/mnist_line.model', 'wb'))

# ellipse
print('')
print('')
print("======Loading ellipse training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    args.train_transform_folder + "/ellipse-image", args.train_transform_folder + "/ellipse-labels")
print("loaded", N)
mlp7 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='tanh', max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp7.fit(training_digits, training_labels)
print("Training set score: %f" % mlp7.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    args.test_transform_folder + "/ellipse-image", args.test_transform_folder + "/ellipse-labels")
print("loaded", n)
print("Test set score: %f" % mlp7.score(test_digits, test_labels))
pickle.dump(mlp7, open('./models/mnist_ellipse.model', 'wb'))

# circle
print('')
print('')
print("======Loading circle training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    args.train_transform_folder + "/circle-image", args.train_transform_folder + "/circle-labels")
print("loaded", N)
mlp8 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='logistic', max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp8.fit(training_digits, training_labels)
print("Training set score: %f" % mlp8.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    args.test_transform_folder + "/circle-image", args.test_transform_folder + "/circle-labels")
print("loaded", n)
print("Test set score: %f" % mlp8.score(test_digits, test_labels))
pickle.dump(mlp8, open('./models/mnist_circle.model', 'wb'))


# ellipse or circle
print('')
print('')
print("======Loading ellipse or circle training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    args.train_transform_folder + "/ellipse_circle-image", args.train_transform_folder + "/ellipse_circle-labels")
print("loaded", N)
mlp9 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='logistic', max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp9.fit(training_digits, training_labels)
print("Training set score: %f" % mlp9.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    args.test_transform_folder + "/ellipse_circle-image", args.test_transform_folder + "/ellipse_circle-labels")
print("loaded", n)
print("Test set score: %f" % mlp9.score(test_digits, test_labels))
pickle.dump(mlp9, open('./models/mnist_ellipse_circle.model', 'wb'))

