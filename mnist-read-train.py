from load_mnist_data import load_mnist_float
from sklearn.neural_network import MLPClassifier

# mlp hidden layers
#hidden_layers = (50,)
hidden_layers = (100, 200)
act_func = "logistic" #relu default, tanh and logistic alternatives

print('hidden layers:', hidden_layers)
print('activation function:', act_func)

home_folder = "/Users/pcw"

# original
print("======Loading original training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    home_folder + "/mnist/download/train-images-idx3-ubyte", home_folder + "/mnist/download/train-labels-idx1-ubyte")
print("loaded", N)
mlp = MLPClassifier(hidden_layer_sizes=hidden_layers, activation=act_func, max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp.fit(training_digits, training_labels)
print("Training set score: %f" % mlp.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/download/t10k-images-idx3-ubyte", home_folder + "/mnist/download/t10k-labels-idx1-ubyte")
print("loaded", n)
print("Test set score: %f" % mlp.score(test_digits, test_labels))


# skeleton
print('')
print('')
print("======Loading skeleton training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    home_folder + "/mnist/train-transform/skel-image", home_folder + "/mnist/train-transform/skel-labels")
print("loaded", N)
mlp1 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation=act_func, max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp1.fit(training_digits, training_labels)
print("Training set score: %f" % mlp1.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/skel-image", home_folder + "/mnist/test-transform/skel-labels")
print("loaded", n)
print("Test set score: %f" % mlp1.score(test_digits, test_labels))


# endpoints
print('')
print('')
print("======Loading endpoints training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    home_folder + "/mnist/train-transform/endpoint-image", home_folder + "/mnist/train-transform/endpoint-labels")
print("loaded", N)
mlp2 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation=act_func, max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp2.fit(training_digits, training_labels)
print("Training set score: %f" % mlp2.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/endpoint-image", home_folder + "/mnist/test-transform/endpoint-labels")
print("loaded", n)
print("Test set score: %f" % mlp2.score(test_digits, test_labels))


# crossings
print('')
print('')
print("======Loading crossings training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    home_folder + "/mnist/train-transform/crossing-image", home_folder + "/mnist/train-transform/crossing-labels")
print("loaded", N)
mlp3 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation=act_func, max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp3.fit(training_digits, training_labels)
print("Training set score: %f" % mlp3.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/crossing-image", home_folder + "/mnist/test-transform/crossing-labels")
print("loaded", n)
print("Test set score: %f" % mlp3.score(test_digits, test_labels))


# fill
print('')
print('')
print("======Loading fill training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    home_folder + "/mnist/train-transform/fill-image", home_folder + "/mnist/train-transform/fill-labels")
print("loaded", N)
mlp4 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation=act_func, max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp4.fit(training_digits, training_labels)
print("Training set score: %f" % mlp4.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/fill-image", home_folder + "/mnist/test-transform/fill-labels")
print("loaded", n)
print("Test set score: %f" % mlp4.score(test_digits, test_labels))


# skel fill
print('')
print('')
print("======Loading skel fill training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    home_folder + "/mnist/train-transform/skel-fill-image", home_folder + "/mnist/train-transform/skel-fill-labels")
print("loaded", N)
mlp5 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation=act_func, max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp5.fit(training_digits, training_labels)
print("Training set score: %f" % mlp5.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/skel-fill-image", home_folder + "/mnist/test-transform/skel-fill-labels")
print("loaded", n)
print("Test set score: %f" % mlp5.score(test_digits, test_labels))


# lines
print('')
print('')
print("======Loading line training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    home_folder + "/mnist/train-transform/line-image", home_folder + "/mnist/train-transform/line-labels")
print("loaded", N)
mlp6 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation=act_func, max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp6.fit(training_digits, training_labels)
print("Training set score: %f" % mlp6.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/line-image", home_folder + "/mnist/test-transform/line-labels")
print("loaded", n)
print("Test set score: %f" % mlp6.score(test_digits, test_labels))


# ellipse
print('')
print('')
print("======Loading ellipse training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    home_folder + "/mnist/train-transform/ellipse-image", home_folder + "/mnist/train-transform/ellipse-labels")
print("loaded", N)
mlp7 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation=act_func, max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp7.fit(training_digits, training_labels)
print("Training set score: %f" % mlp7.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/ellipse-image", home_folder + "/mnist/test-transform/ellipse-labels")
print("loaded", n)
print("Test set score: %f" % mlp7.score(test_digits, test_labels))


# circle
print('')
print('')
print("======Loading circle training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    home_folder + "/mnist/train-transform/circle-image", home_folder + "/mnist/train-transform/circle-labels")
print("loaded", N)
mlp8 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation=act_func, max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp8.fit(training_digits, training_labels)
print("Training set score: %f" % mlp8.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/circle-image", home_folder + "/mnist/test-transform/circle-labels")
print("loaded", n)
print("Test set score: %f" % mlp8.score(test_digits, test_labels))


# ellipse or circle
print('')
print('')
print("======Loading ellipse or circle training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    home_folder + "/mnist/train-transform/ellipse_circle-image", home_folder + "/mnist/train-transform/ellipse_circle-labels")
print("loaded", N)
mlp9 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation=act_func, max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp9.fit(training_digits, training_labels)
print("Training set score: %f" % mlp9.score(training_digits, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/ellipse_circle-image", home_folder + "/mnist/test-transform/ellipse_circle-labels")
print("loaded", n)
print("Test set score: %f" % mlp9.score(test_digits, test_labels))