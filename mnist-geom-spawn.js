const { spawn } = require('child_process');
const pino = require('pino');

const logger = pino(
  {
    prettyPrint: {
      colorize: true,
      levelFirst: true,
      translateTime: "yyyy-dd-mm, h:MM:ss TT",
    },
  },
  pino.destination("./geom-logger.log")
);


let names = [
    /*'base-image',
    'skeleton',
    'endpoint',
    'crossing',
    'fill',*/
    'skel-fill',
    'line',
    'ellipse',
    'circle',
    'ellipse-circle'];
let trainImages = [
    //"/home/pcw/mnist/download/train-images-idx3-ubyte",
    //"/home/pcw/mnist/train-transform/skel-image",
    //"/home/pcw/mnist/train-transform/endpoint-image",
    //"/home/pcw/mnist/train-transform/crossing-image",
    //"/home/pcw/mnist/train-transform/fill-image",
    "/home/pcw/mnist/train-transform/skel-fill-image",
    "/home/pcw/mnist/train-transform/line-image",
    "/home/pcw/mnist/train-transform/ellipse-image",
    "/home/pcw/mnist/train-transform/circle-image",
    "/home/pcw/mnist/train-transform/ellipse_circle-image"
];
let testImages = [
    //"/home/pcw/mnist/download/t10k-images-idx3-ubyte",
    //"/home/pcw/mnist/test-transform/skel-image",
    //"/home/pcw/mnist/test-transform/endpoint-image",
    //"/home/pcw/mnist/test-transform/crossing-image",
    //"/home/pcw/mnist/test-transform/fill-image",
    "/home/pcw/mnist/test-transform/skel-fill-image",
    "/home/pcw/mnist/test-transform/line-image",
    "/home/pcw/mnist/test-transform/ellipse-image",
    "/home/pcw/mnist/test-transform/circle-image",
    "/home/pcw/mnist/test-transform/ellipse_circle-image"
];

let batches = [];

for (let i = 0; i < names.length; i++) {
    batches.push({
        name: names[i],
        trainImages: trainImages[i],
        trainLabels: '/home/pcw/mnist/download/train-labels-idx1-ubyte',
        testImages: testImages[i],
        testLabels: '/home/pcw/mnist/download/t10k-labels-idx1-ubyte'
    });
}


function runBatch(batchList) {
    batch = batchList.shift();

    //const batchProc = spawn(`python mnist_find_geometry.py -n ${batch.name} -i ${batch.trainImages} -l ${batch.trainLabels} -t ${batch.testImages} -e ${batch.testLabels} -o test.out`);

    const batchProc = spawn(
        'python3', [
            '-u',
            'mnist_find_geometry.py',
            '-n',
            batch.name,
            '-i',
            batch.trainImages,
            '-l',
            batch.trainLabels,
            '-t',
            batch.testImages,
            '-e',
            batch.testLabels,
            '-o',
            `./geom/${batch.name}.out`
        ]);

/*
        const batchProc = spawn(
            'python3', [
                '--version'
            ]);
*/


    batchProc.stdout.on('data', (data) => {
        str = data.toString().replace('\n', '') //console.log(Buffer.isBuffer(data));
        //data = data.replace('\r\n', '');
        if (str.length > 0) {
            logger.info(`stdout: ${str}`);
        }
    });

    batchProc.stderr.on('data', (data) => {
        logger.error(`stderr: ${data}`);
    });

    batchProc.on('close', (code) => {
        logger.info(`child process exited with code ${code}`);
        if (batchList.length > 0) {
            setImmediate(runBatch, batchList);
        }
    });
}

runBatch(batches);