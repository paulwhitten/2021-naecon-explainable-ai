import json
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.ticker as mticker 

with open('mean-plot/6-mean-digit.json') as f:
  data = json.load(f)

y = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
x = [1, 2, 3, 4, 5, 6, 7, 8, 9]
z = data

print(type(data))

X, Y = np.meshgrid(x, y)
Z = np.reshape(z, X.shape)  # Z.shape must be equal to X.shape = Y.shape

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.plot_surface(X, Y, Z, cmap='winter')

ax.set_xlabel('Properties')
ax.set_ylabel('Digits')
ax.set_zlabel('Mean')
plt.xticks(x, x)
plt.yticks(y, y)
plt.show()