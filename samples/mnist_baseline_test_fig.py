import argparse
from load_mnist_data import load_mnist
import matplotlib.pyplot as plt
import numpy as np

parser = argparse.ArgumentParser(description='loads a model, data, and labels then does prediction.')
parser.add_argument('-i', '--imageFile', 
                    help='The mnist image input file')
parser.add_argument('-l', '--labelFile', 
                    help='The mnist label input file')
parser.add_argument('-t', '--title', 
                    help='The title of the plot')
parser.add_argument('-o', '--outputFolder', 
                    help='The folder to output')
args = parser.parse_args()

n, rows, columns, digits, labels = load_mnist(args.imageFile, args.labelFile)
print("loaded", n, "from", args.imageFile, "and", args.labelFile)

frequency = [0] * 10
i = 0
for label in labels:
    frequency[label] = frequency[label] + 1
    i = i + 1

values = []
for count in frequency:
    values.append(count / i)

x = np.arange(10)
fig, ax = plt.subplots()
plt.ylim(0.0, 1.0)
rects = ax.bar(x, height=values)
plt.xticks(x, ['0','1','2','3','4','5','6','7','8','9'])
plt.title("mnist " + args.title + " distribution of labels")
for rect in rects:
    height = rect.get_height()
    ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%.2f' % height,
                ha='center', va='bottom')
plt.savefig(args.outputFolder + "/" + args.title + "-frequency.png")
#plt.show()

