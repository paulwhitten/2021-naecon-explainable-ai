from skimage.morphology import dilation
from skimage.morphology import disk
from skimage.io import imread, imshow
from matplotlib import pyplot as plt
from skimage.morphology import medial_axis, skeletonize

img = imread('~/dev/xai/data-py/img/0-33.png')

dil = dilation(img, selem=disk(3))

imshow(skeletonize(dil, method='lee'))
plt.show()