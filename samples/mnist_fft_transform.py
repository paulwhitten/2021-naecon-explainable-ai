from skimage.io import imread, imshow
from matplotlib import pyplot as plt
from scipy import fftpack
import numpy as np

img = imread('~/dev/xai/data-py/img/0-1.png')

dft = np.fft.fft2(img)

fft_img = fftpack.fft2(img)
abs_img = np.abs(fft_img)
abs_img *= 255 / abs_img.max()
fft_im_uint8 = abs_img.astype(np.uint8)
imshow(fft_im_uint8) # np.abs(fft_img)
plt.show()

print(fft_im_uint8)

print(dft)