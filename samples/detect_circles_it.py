import matplotlib.pyplot as plt

from skimage import data, color, img_as_ubyte
from skimage.feature import canny
from skimage.transform import hough_ellipse
from skimage.draw import ellipse_perimeter
from skimage import io
import numpy as np

#im_name = "0-33.png"

for i in range(38):

    im_name = "1-" + str(i) + ".png"

    # Load picture, convert to grayscale and detect edges
    image_rgb = io.imread("../data-py/skel-lee/" + im_name)
    edges = image = io.imread("../data-py/skel-lee/" + im_name)
    empty = np.zeros([28,28], np.uint8)

    # Perform a Hough Transform
    # The accuracy corresponds to the bin size of a major axis.
    # The value is chosen in order to get a single high accumulator.
    # The threshold eliminates low accumulators
    result = hough_ellipse(edges)#, threshold=20)
                        #min_size=5, max_size=23)
    if len(result) > 0:
        result.sort(order='accumulator')

        # Estimated parameters for the ellipse
        best = list(result[-1])
        yc, xc, a, b = [int(round(x)) for x in best[1:5]]
        orientation = best[5]

        print(im_name, "orientation", orientation, yc, xc, a, b)

        # Draw the ellipse on the original image
        cy, cx = ellipse_perimeter(yc, xc, a, b, orientation)
        # Draw the edge (white) and the resulting ellipse (red)
        for j in range(len(cy)):
            if (cy[j] > 0 and cy[j] < 28 and cx[j] > 0 and cx[j] < 28):
                empty[cy[j], cx[j]] = 255
    else:
        print(im_name, "no ellipse")

    #fig2, (ax1, ax2) = plt.subplots(ncols=2, nrows=1, figsize=(8, 4),
    #                               sharex=True, sharey=True)

    #ax1.set_title('Original picture')
    #ax1.imshow(image_rgb)

    #ax2.set_title('Ellipse')
    #ax2.imshow(empty)

#plt.show()