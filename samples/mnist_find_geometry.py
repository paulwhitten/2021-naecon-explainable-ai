from load_mnist_data import load_mnist_float
from sklearn.neural_network import MLPClassifier
import numpy as np
import argparse

# adapterd from mnist_train_multiple.py

INCREMENT = 50

parser = argparse.ArgumentParser(description='Saves skeleton data and writes images')
parser.add_argument('-n', '--name', 
                    help='The name of the transform')
parser.add_argument('-i', '--trainingImage', 
                    help='The mnist training image file')
parser.add_argument('-l', '--trainingLabel', 
                    help='The mnist training label file')
parser.add_argument('-t', '--testImage', 
                    help='The mnist test image file')
parser.add_argument('-e', '--testLabel', 
                    help='The mnist test label file')
parser.add_argument('-o', '--output', 
                    help='The output file.')  
args = parser.parse_args()

print("Loading training data for " + args.name)
N, rows, columns, training_digits, training_labels = load_mnist_float(
    args.trainingImage, args.trainingLabel)
print("loaded", N)

print("Loading test data")
n, trows, tcolumns, test_digits, test_labels = load_mnist_float(
    args.testImage, args.testLabel)
print("loaded", n)

hidden_neurons = INCREMENT

best_test_score = 0.0

while hidden_neurons < 961:

    for i in range(3): # use i to select activation function
        activation = 'relu' # default
        if i == 1:
            activation = 'logistic'
        elif i == 2:
            activation = 'tanh'

        hidden_layers = (hidden_neurons,)

        print('hidden layers:', hidden_layers)
    
        mlp = MLPClassifier(hidden_layer_sizes=hidden_layers, activation=activation, max_iter=500, alpha=1e-4,
                            solver='sgd', verbose=10, random_state=1,
                            learning_rate_init=.1)

        mlp.fit(training_digits, training_labels)
        print(args.name)
        print('hidden layers:', hidden_layers)
        print('activation: {}'.format(activation))
        print("Training set score: %f" % mlp.score(training_digits, training_labels))
        test_score = mlp.score(test_digits, test_labels)
        print("Test set score: %f" % test_score)
        if test_score > best_test_score:
            best_props = "best properties for " + args.name + " = hidden: (" + ','.join(str(p) for p in hidden_layers) + ") activation: " + activation + " score: " + str(test_score)
            best_test_score = test_score
            print("best_props: " + best_props)
    hidden_neurons = hidden_neurons + INCREMENT

print(best_props)

f = open(args.output, "w")
f.write(best_props)
f.close()

#pickle.dump(mlp, open('mnist_raw.model', 'wb'))

# # skeleton
# print('')
# print('')
# print("======Loading skeleton training data.======")
# N, rows, columns, training_digits, training_labels = load_mnist_float(
#     home_folder + "/mnist/train-transform/skel-image", home_folder + "/mnist/train-transform/skel-labels")
# print("loaded", N)
# mlp1 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='relu', max_iter=500, alpha=1e-4,
#                     solver='sgd', verbose=10, random_state=1,
#                     learning_rate_init=.1)

# mlp1.fit(training_digits, training_labels)
# print("Training set score: %f" % mlp1.score(training_digits, training_labels))

# print("Loading test data.")
# n, rows, columns, test_digits, test_labels = load_mnist_float(
#     home_folder + "/mnist/test-transform/skel-image", home_folder + "/mnist/test-transform/skel-labels")
# print("loaded", n)
# print("Test set score: %f" % mlp1.score(test_digits, test_labels))
# pickle.dump(mlp1, open('mnist_skeleton.model', 'wb'))


# # endpoints
# print('')
# print('')
# print("======Loading endpoints training data.======")
# N, rows, columns, training_digits, training_labels = load_mnist_float(
#     home_folder + "/mnist/train-transform/endpoint-image", home_folder + "/mnist/train-transform/endpoint-labels")
# print("loaded", N)
# mlp2 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='logistic', max_iter=500, alpha=1e-4,
#                     solver='sgd', verbose=10, random_state=1,
#                     learning_rate_init=.1)

# mlp2.fit(training_digits, training_labels)
# print("Training set score: %f" % mlp2.score(training_digits, training_labels))

# print("Loading test data.")
# n, rows, columns, test_digits, test_labels = load_mnist_float(
#     home_folder + "/mnist/test-transform/endpoint-image", home_folder + "/mnist/test-transform/endpoint-labels")
# print("loaded", n)
# print("Test set score: %f" % mlp2.score(test_digits, test_labels))
# pickle.dump(mlp2, open('mnist_endpoint.model', 'wb'))


# # crossings
# print('')
# print('')
# print("======Loading crossings training data.======")
# N, rows, columns, training_digits, training_labels = load_mnist_float(
#     home_folder + "/mnist/train-transform/crossing-image", home_folder + "/mnist/train-transform/crossing-labels")
# print("loaded", N)
# mlp3 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='tanh', max_iter=500, alpha=1e-4,
#                     solver='sgd', verbose=10, random_state=1,
#                     learning_rate_init=.1)

# mlp3.fit(training_digits, training_labels)
# print("Training set score: %f" % mlp3.score(training_digits, training_labels))

# print("Loading test data.")
# n, rows, columns, test_digits, test_labels = load_mnist_float(
#     home_folder + "/mnist/test-transform/crossing-image", home_folder + "/mnist/test-transform/crossing-labels")
# print("loaded", n)
# print("Test set score: %f" % mlp3.score(test_digits, test_labels))
# pickle.dump(mlp3, open('mnist_crossing.model', 'wb'))


# # fill
# print('')
# print('')
# print("======Loading fill training data.======")
# N, rows, columns, training_digits, training_labels = load_mnist_float(
#     home_folder + "/mnist/train-transform/fill-image", home_folder + "/mnist/train-transform/fill-labels")
# print("loaded", N)
# mlp4 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='tanh', max_iter=500, alpha=1e-4,
#                     solver='sgd', verbose=10, random_state=1,
#                     learning_rate_init=.1)

# mlp4.fit(training_digits, training_labels)
# print("Training set score: %f" % mlp4.score(training_digits, training_labels))

# print("Loading test data.")
# n, rows, columns, test_digits, test_labels = load_mnist_float(
#     home_folder + "/mnist/test-transform/fill-image", home_folder + "/mnist/test-transform/fill-labels")
# print("loaded", n)
# print("Test set score: %f" % mlp4.score(test_digits, test_labels))
# pickle.dump(mlp4, open('mnist_fill.model', 'wb'))


# # skel fill
# print('')
# print('')
# print("======Loading skel fill training data.======")
# N, rows, columns, training_digits, training_labels = load_mnist_float(
#     home_folder + "/mnist/train-transform/skel-fill-image", home_folder + "/mnist/train-transform/skel-fill-labels")
# print("loaded", N)
# mlp5 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='tanh', max_iter=500, alpha=1e-4,
#                     solver='sgd', verbose=10, random_state=1,
#                     learning_rate_init=.1)

# mlp5.fit(training_digits, training_labels)
# print("Training set score: %f" % mlp5.score(training_digits, training_labels))

# print("Loading test data.")
# n, rows, columns, test_digits, test_labels = load_mnist_float(
#     home_folder + "/mnist/test-transform/skel-fill-image", home_folder + "/mnist/test-transform/skel-fill-labels")
# print("loaded", n)
# print("Test set score: %f" % mlp5.score(test_digits, test_labels))
# pickle.dump(mlp5, open('mnist_skel_fill.model', 'wb'))


# # lines
# print('')
# print('')
# print("======Loading line training data.======")
# N, rows, columns, training_digits, training_labels = load_mnist_float(
#     home_folder + "/mnist/train-transform/line-image", home_folder + "/mnist/train-transform/line-labels")
# print("loaded", N)
# mlp6 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='relu', max_iter=500, alpha=1e-4,
#                     solver='sgd', verbose=10, random_state=1,
#                     learning_rate_init=.1)

# mlp6.fit(training_digits, training_labels)
# print("Training set score: %f" % mlp6.score(training_digits, training_labels))

# print("Loading test data.")
# n, rows, columns, test_digits, test_labels = load_mnist_float(
#     home_folder + "/mnist/test-transform/line-image", home_folder + "/mnist/test-transform/line-labels")
# print("loaded", n)
# print("Test set score: %f" % mlp6.score(test_digits, test_labels))
# pickle.dump(mlp6, open('mnist_line.model', 'wb'))


# # ellipse
# print('')
# print('')
# print("======Loading ellipse training data.======")
# N, rows, columns, training_digits, training_labels = load_mnist_float(
#     home_folder + "/mnist/train-transform/ellipse-image", home_folder + "/mnist/train-transform/ellipse-labels")
# print("loaded", N)
# mlp7 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='tanh', max_iter=500, alpha=1e-4,
#                     solver='sgd', verbose=10, random_state=1,
#                     learning_rate_init=.1)

# mlp7.fit(training_digits, training_labels)
# print("Training set score: %f" % mlp7.score(training_digits, training_labels))

# print("Loading test data.")
# n, rows, columns, test_digits, test_labels = load_mnist_float(
#     home_folder + "/mnist/test-transform/ellipse-image", home_folder + "/mnist/test-transform/ellipse-labels")
# print("loaded", n)
# print("Test set score: %f" % mlp7.score(test_digits, test_labels))
# pickle.dump(mlp7, open('mnist_ellipse.model', 'wb'))


# # circle
# print('')
# print('')
# print("======Loading circle training data.======")
# N, rows, columns, training_digits, training_labels = load_mnist_float(
#     home_folder + "/mnist/train-transform/circle-image", home_folder + "/mnist/train-transform/circle-labels")
# print("loaded", N)
# mlp8 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='logistic', max_iter=500, alpha=1e-4,
#                     solver='sgd', verbose=10, random_state=1,
#                     learning_rate_init=.1)

# mlp8.fit(training_digits, training_labels)
# print("Training set score: %f" % mlp8.score(training_digits, training_labels))

# print("Loading test data.")
# n, rows, columns, test_digits, test_labels = load_mnist_float(
#     home_folder + "/mnist/test-transform/circle-image", home_folder + "/mnist/test-transform/circle-labels")
# print("loaded", n)
# print("Test set score: %f" % mlp8.score(test_digits, test_labels))
# pickle.dump(mlp8, open('mnist_circle.model', 'wb'))


# # ellipse or circle
# print('')
# print('')
# print("======Loading ellipse or circle training data.======")
# N, rows, columns, training_digits, training_labels = load_mnist_float(
#     home_folder + "/mnist/train-transform/ellipse_circle-image", home_folder + "/mnist/train-transform/ellipse_circle-labels")
# print("loaded", N)
# mlp9 = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='logistic', max_iter=500, alpha=1e-4,
#                     solver='sgd', verbose=10, random_state=1,
#                     learning_rate_init=.1)

# mlp9.fit(training_digits, training_labels)
# print("Training set score: %f" % mlp9.score(training_digits, training_labels))

# print("Loading test data.")
# n, rows, columns, test_digits, test_labels = load_mnist_float(
#     home_folder + "/mnist/test-transform/ellipse_circle-image", home_folder + "/mnist/test-transform/ellipse_circle-labels")
# print("loaded", n)
# print("Test set score: %f" % mlp9.score(test_digits, test_labels))
# pickle.dump(mlp9, open('mnist_ellipse_circle.model', 'wb'))
