from load_mnist_data import load_mnist_float
from load_mnist_data import load_mnist
import pickle
import json

'''
Executes all models saving results in a json file
'''

property_descriptions = ['skeleton', 'circle', 'crossing', 'ellipse', 'ellipseCircle', 'endpoint', 'fill', 'line', 'skeletonFill']

model_files = ['mnist_skeleton.model', 'mnist_circle.model', 'mnist_crossing.model', 'mnist_ellipse.model', 'mnist_ellipse_circle.model',
  'mnist_endpoint.model', 'mnist_fill.model', 'mnist_line.model', 'mnist_skel_fill.model']

image_files = ['/home/pcw/mnist/confounder-transform/skel-image', '/home/pcw/mnist/confounder-transform/circle-image', '/home/pcw/mnist/confounder-transform/crossing-image',
  '/home/pcw/mnist/confounder-transform/ellipse-image', '/home/pcw/mnist/confounder-transform/ellipse_circle-image', '/home/pcw/mnist/confounder-transform/endpoint-image',
  '/home/pcw/mnist/confounder-transform/fill-image', '/home/pcw/mnist/confounder-transform/line-image', '/home/pcw/mnist/confounder-transform/skel-fill-image']

label_files = ['/home/pcw/mnist/confounder/labels', '/home/pcw/mnist/confounder/labels', '/home/pcw/mnist/confounder/labels', '/home/pcw/mnist/confounder/labels',
  '/home/pcw/mnist/confounder/labels', '/home/pcw/mnist/confounder/labels', '/home/pcw/mnist/confounder/labels',
  '/home/pcw/mnist/confounder/labels', '/home/pcw/mnist/confounder/labels']

n, rows, cols, raw_digits, raw_labels = load_mnist('/home/pcw/mnist/confounder/images', '/home/pcw/mnist/confounder/labels')
int_labels = []
for label in raw_labels:
    int_labels.append(int(label))

print("loaded raw")

models = []
images = []
labels = []
scores = []

for i in range(len(model_files)):
    models.append(pickle.load(open(model_files[i], 'rb')))
    i_n, i_rows, i_columns, i_digits, i_labels = load_mnist_float(image_files[i], label_files[i])
    images.append(i_digits)
    labels.append(i_labels)
    scores.append(models[i].score(i_digits, i_labels))

print("loaded models and images")

property_results = []
proba_results = []
for i in range(len(images[0])):
    property_predictions = []
    probas = []
    for j in range(len(model_files)):
        prediction = models[j].predict([images[j][i]])
        proba = models[j].predict_proba([images[j][i]])
        predictions = []
        for p in prediction[0]:
            predictions.append(int(p))
        property_predictions.append(predictions)
        probas.append(proba[0].tolist())
    property_results.append(property_predictions)
    proba_results.append(probas)

output_json = {}

output_json['predictions'] = property_results
output_json['labels'] = int_labels
output_json['propertyDescriptions'] = property_descriptions
output_json['probabilitiyPredictions'] = proba_results
output_json['propertyScores'] = scores

with open('all-confounders.json', 'w') as outfile:
    json.dump(output_json, outfile)