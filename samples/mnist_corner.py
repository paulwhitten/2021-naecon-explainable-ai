from skimage.io import imread, imshow
from skimage.feature import corner_fast, corner_harris, corner_kitchen_rosenfeld, corner_moravec, corner_peaks
import numpy as np
from matplotlib import pyplot as plt
from math import floor


img = imread('~/devel/data-py/raw/9-10.png')

corner = corner_harris(img)

peaks = corner_peaks(corner, indices=False)

max = np.max(corner)

max_thresh = max - (max * 0.10)

new_image = np.zeros((img.shape[0], img.shape[1]), np.uint8)

for y in range(img.shape[0]):
        for x in range(img.shape[1]):
            if max_thresh < corner[y,x]:
                new_image[y,x] = 255

imshow(corner)
plt.show()