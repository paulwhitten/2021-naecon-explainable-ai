from skimage.io import imread, imshow
from skimage.morphology import convex_hull_image
from matplotlib import pyplot as plt
import numpy as np

img = imread('~/dev/xai/data-py/skel-lee/7-2.png')

chull = convex_hull_image(img)

new_image = np.zeros((img.shape[0], img.shape[1]), np.uint8)

for y in range(img.shape[0]):
        for x in range(img.shape[1]):
            if chull[y,x]:
                new_image[y,x] = 255

imshow(new_image)
plt.show()