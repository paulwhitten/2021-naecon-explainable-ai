import argparse

parser = argparse.ArgumentParser(description='Read mnist files')
parser.add_argument('-s', '--saveImages', action='store_const',
                    const=True, default=False, 
                    help='Flag to save images, default does not save.')

args = parser.parse_args()

if (args.saveImages):
    print("do it!")
else:
    print("nothing")
