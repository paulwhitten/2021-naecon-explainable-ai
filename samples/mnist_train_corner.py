from load_mnist_data import load_mnist
from sklearn.neural_network import MLPClassifier
from skimage.feature import corner_shi_tomasi
import numpy as np
import pickle

# mlp hidden layers
#hidden_layers = (50,)
hidden_layers = (100, 200)

print('hidden layers:', hidden_layers)

home_folder = "/home/pcw"

# original
print("======Loading original training data.======")
N, rows, columns, training_digits, training_labels = load_mnist(
    home_folder + "/mnist/download/train-images-idx3-ubyte", home_folder + "/mnist/download/train-labels-idx1-ubyte")
print("loaded", N)

corners = []

for d in training_digits:
    i = np.reshape(d, (-1, 28))
    corner = corner_shi_tomasi(i)
    max = np.max(corner)
    min = np.min(corner)
    corner *= 2.0/(min-max)
    corner -= 1.0
    # max = np.max(corner)
    # max_thresh = max - (max * 0.10)
    # new_image = np.zeros((i.shape[0], i.shape[1]), np.uint8)
    # for y in range(i.shape[0]):
    #         for x in range(i.shape[1]):
    #             if max_thresh < corner[y,x]:
    #                 new_image[y,x] = 255
    # new_image = new_image.astype(float)
    # new_image *= 255.0 / new_image.max()
    corners.append(corner.flatten())

mlp = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='tanh', max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp.fit(corners, training_labels)
print("Training set score: %f" % mlp.score(corners, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist(
    home_folder + "/mnist/download/t10k-images-idx3-ubyte", home_folder + "/mnist/download/t10k-labels-idx1-ubyte")

test_corners = []
for d in test_digits:
    i = np.reshape(d, (-1, 28))
    corner = corner_shi_tomasi(i)
    max = np.max(corner)
    min = np.min(corner)
    corner *= 2.0/(min-max)
    corner -= 1.0
    # max = np.max(corner)
    # max_thresh = max - (max * 0.10)
    # new_image = np.zeros((i.shape[0], i.shape[1]), np.uint8)
    # for y in range(i.shape[0]):
    #         for x in range(i.shape[1]):
    #             if max_thresh < corner[y,x]:
    #                 new_image[y,x] = 255
    # new_image = new_image.astype(float)
    # new_image *= 255.0 / new_image.max()
    test_corners.append(corner.flatten())
print("loaded", n)
print("Test set score: %f" % mlp.score(test_corners, test_labels))

