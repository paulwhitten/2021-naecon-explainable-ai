from load_mnist_data import load_mnist
from sklearn.neural_network import MLPClassifier
from skimage.filters import prewitt # or sobel
import numpy as np
import pickle

# mlp hidden layers
#hidden_layers = (50,)
hidden_layers = (100, 200)

print('hidden layers:', hidden_layers)

home_folder = "/home/pcw"

# original
print("======Loading original training data.======")
N, rows, columns, training_digits, training_labels = load_mnist(
    home_folder + "/mnist/download/train-images-idx3-ubyte", home_folder + "/mnist/download/train-labels-idx1-ubyte")
print("loaded", N)

training_images = []

for d in training_digits:
    i = np.reshape(d, (-1, 28))
    edges = prewitt(i)
    training_images.append(edges.flatten())

mlp = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='relu', max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp.fit(training_images, training_labels)
print("Training set score: %f" % mlp.score(training_images, training_labels))

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist(
    home_folder + "/mnist/download/t10k-images-idx3-ubyte", home_folder + "/mnist/download/t10k-labels-idx1-ubyte")

test_images = []
for d in test_digits:
    i = np.reshape(d, (-1, 28))
    edges = prewitt(i)
    test_images.append(edges.flatten())
print("loaded", n)
print("Test set score: %f" % mlp.score(test_images, test_labels))

