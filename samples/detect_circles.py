import matplotlib.pyplot as plt

from skimage import data, color, img_as_ubyte
from skimage.feature import canny
from skimage.transform import hough_ellipse, hough_circle, hough_circle_peaks
from skimage.draw import ellipse_perimeter, circle_perimeter
from skimage import io
import numpy as np

def get_neighbors(loc, img):
    rows = img.shape[0]
    columns = img.shape[1]
    neighbors = []
    if (loc[1] - 1 >= 0): # check north
        neighbors.append((loc[0], loc[1] - 1))
    if (loc[1] + 1 < rows): # check south
        neighbors.append((loc[0], loc[1] + 1))
    if (loc[0] + 1 < columns): # check east
        neighbors.append((loc[0] + 1, loc[1]))
    if (loc[0] - 1 >= 0): # check west
        neighbors.append((loc[0] - 1, loc[1]))
    return neighbors

def fill_set_contains_border(img, fill_set):
    rows = img.shape[0]
    columns = img.shape[1]
    if ((0, 0) in fill_set or (0, rows - 1) in fill_set or (columns - 1, 0) in fill_set or (columns - 1, rows - 1) in fill_set) :
        return True
    else:
        for loc in fill_set:
            if (loc[0] == 0 or loc[0] == columns - 1 or loc[1] == 0 or loc[1] == rows - 1):
                return True
    return False

def flood_fill_loops(img, threshold):
    visited = dict()
    loops = []
    for y in range(img.shape[0]):
        for x in range(img.shape[1]):
            if (not (x, y) in visited.keys()):
                if (threshold > img[y, x]):
                    to_fill = [(x, y)]
                    filled = []
                    while len(to_fill) > 0:
                        #print("len(to_fill):", len(to_fill))
                        loc = to_fill.pop(0)
                        #print("len(to_fill):", len(to_fill))
                        if (img[loc[1], loc[0]] < threshold):
                            filled.append(loc)
                            visited[loc] = True
                            neighbors = get_neighbors(loc, img)
                            for neighbor in neighbors:
                                if (not neighbor in visited.keys() and not neighbor in to_fill):
                                    #print("adding", neighbor)
                                    to_fill.append(neighbor)
                        else:
                            visited[loc] = True
                    if (not fill_set_contains_border(img, filled)):
                        loops.append(filled)
                else:
                    visited[(x, y)] = True
    fill_img = np.zeros((img.shape[0], img.shape[1]), np.uint8)
    for loop_set in loops:
        for pixel in loop_set:
            fill_img[pixel[1], pixel[0]] = 255
    return fill_img

def flood_fill_overlap(fill1, fill2):
    for y in range(fill1.shape[0]):
        for x in range(fill1.shape[1]):
            if (fill1[y, x] == 255 and fill2[y, x] == 255):
                return True
    return False

im_name = "2-8.png"#"8-11.png" #"8-4.png"

print("image", im_name)

image_rgb = io.imread("../data-py/skel-lee/" + im_name)
edges = image = io.imread("../data-py/skel-lee/" + im_name)
ellipse = np.zeros([28,28], np.uint8)
circle0 = np.zeros([28,28], np.uint8)
circle1 = np.zeros([28,28], np.uint8)
circle2 = np.zeros([28,28], np.uint8)

a = 0
b = 0

# Perform a Hough Transform
# The accuracy corresponds to the bin size of a major axis.
# The value is chosen in order to get a single high accumulator.
# The threshold eliminates low accumulators
result = hough_ellipse(edges, threshold=5)#, threshold=20)
                       #min_size=5, max_size=23)

if (len(result) > 0):
    result.sort(order='accumulator')
    print("result", result)
    # Estimated parameters for the ellipse
    best = list(result[-1])
    yc, xc, a, b = [int(round(x)) for x in best[1:5]]
    orientation = best[5]

    print(im_name, "orientation", orientation, yc, xc, a, b)
    print("ellipse result size:", len(result))

    # Draw the ellipse on the original image
    cy, cx = ellipse_perimeter(yc, xc, a, b, orientation)
    # Draw the edge (white) and the resulting ellipse (red)
    ellipse[cy, cx] = 255

fig2, (ax1, ax2, ax3, ax4, ax5, final) = plt.subplots(ncols=6, nrows=1, figsize=(8, 4),
                                    sharex=True, sharey=True)

# get circles
hough_radii = np.arange(4, 13, 1)
hough_res = hough_circle(edges, hough_radii)

# Select the most prominent 3 circles
accums, cx, cy, radii = hough_circle_peaks(hough_res, hough_radii,
                                           total_num_peaks=3)

circ0y, circ0x = circle_perimeter(cy[0], cx[0], radii[0],
                                    shape=circle0.shape)
circle0[circ0y, circ0x] = 255

circ1y, circ1x = circle_perimeter(cy[1], cx[1], radii[1],
                                    shape=circle0.shape)
circle1[circ1y, circ1x] = 255

circ2y, circ2x = circle_perimeter(cy[2], cx[2], radii[2],
                                    shape=circle0.shape)
circle2[circ2y, circ2x] = 255

print("circle result size:", len(accums), "accums:", accums)

if (len(result) > 0 and a >= 3 and b >= 3):
    fin_im = ellipse
else:
    fin_im = np.zeros([28,28], np.uint8)
    if (accums[0] > 0.430):
        fill0 = flood_fill_loops(circle0, 100)
        fill1 = flood_fill_loops(circle1, 100)
        fill2 = flood_fill_loops(circle2, 100)
        if (not flood_fill_overlap(fill0, fill1) and
            accums[0] > 0.36 and accums[1] > 0.36):
            fin_im[circ0y, circ0x] = 255
            fin_im[circ1y, circ1x] = 255
        elif (not flood_fill_overlap(fill0, fill2) and
            accums[0] > 0.36 and accums[2] > 0.36):
            fin_im[circ0y, circ0x] = 255
            fin_im[circ2y, circ2x] = 255
        elif (not flood_fill_overlap(fill1, fill2) and
            accums[1] > 0.36 and accums[2] > 0.36):
            fin_im[circ1y, circ1x] = 255
            fin_im[circ2y, circ2x] = 255
        elif (accums[0] > 0.36):
            fin_im[circ0y, circ0x] = 255

ax1.set_title('Original picture')
ax1.imshow(image_rgb)

ax2.set_title('Ellipse')
ax2.imshow(ellipse)

ax3.set_title('Circle0')
ax3.imshow(circle0)

ax4.set_title('Circle1')
ax4.imshow(circle1)

ax5.set_title('Circle2')
ax5.imshow(circle2)

final.set_title('final')
final.imshow(fin_im)

plt.show()