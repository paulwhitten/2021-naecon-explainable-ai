from skimage.io import imread, imshow
from skimage.filters import prewitt
from matplotlib import pyplot as plt
import numpy as np

img = imread('~/devel/data-py/skel-lee/7-2.png')

edges = prewitt(img)

#new_image = edges.astype(float)
#new_image *= 255.0 / new_image.max()

print(edges)

imshow(edges)
plt.show()