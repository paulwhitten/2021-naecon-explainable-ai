import numpy as np
from random import randrange
import pickle
from skimage.io import imshow
from matplotlib import pyplot as plt
from write_mnist_data import write_partial_mnist_data
from skimage import io

print("======loading raw model======")
mlp = pickle.load(open('mnist_raw.model', 'rb'))

c_count = [0,0,0,0,0,0,0,0,0,0]

rows = 28
columns = 28
confounders = 0
confounder_data = []
confounder_labels = bytearray()
for i in range(2000):
    img = np.zeros([rows,columns], np.uint8)
    for y in range(rows):
        for x in range(columns):
            img[y,x] = randrange(255)
    pred = mlp.predict([img.flatten()])
    #print(pred)
    digit = 0
    for k in pred[0]:
        if k == 1:
            print('confounder', digit, mlp.predict_proba([img.flatten()]))
            confounders += 1
            confounder_data.append(img.flatten())
            confounder_labels.append(digit)
            io.imsave("./confounder/" + str(digit) + "-" + str(c_count[digit]) + ".png", img)
            c_count[digit] += 1
        digit += 1

print('confounders:', confounders)
#imshow(img)
#plt.show()
write_partial_mnist_data(confounder_data, confounder_labels, confounders, "./confounder/images", "./confounder/labels")

