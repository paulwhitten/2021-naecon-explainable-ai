import argparse
import struct
from load_mnist_data import load_mnist

# this method will write mnist data to file
#  images is the input  array of images to write
#  labels is the input array of image lables to write
#  count is the number of items to write to the file. expected to be <= sice of input arrays
#  image and label filnames are the files to output to.  overwrites existing data
def write_partial_mnist_data(images, labels, count, image_filename, label_filename):
    rows = 28
    columns = 28
    max = count
    image_buffer_pos = 16
    label_buffer_pos = 8
    image_buffer = bytearray(image_buffer_pos + max * rows * columns)
    label_buffer = bytearray(label_buffer_pos + max)

    struct.pack_into(">iiii", image_buffer, 0, 2051, max, 28, 28)
    struct.pack_into(">ii", label_buffer, 0, 2049, max)
    pixelsInImage = rows * columns
    for x in range(max):
        struct.pack_into("B" * pixelsInImage, image_buffer, image_buffer_pos, *images[x])
        struct.pack_into("B", label_buffer, label_buffer_pos, labels[x])
        image_buffer_pos += pixelsInImage
        label_buffer_pos += 1

    f=open(image_filename,"wb")
    f.write(image_buffer)
    f.close()

    f=open(label_filename,"wb")
    f.write(label_buffer)
    f.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Read mnist files')
    parser.add_argument('-i', '--imageFile', 
                        help='the mnist image file')
    parser.add_argument('-l', '--labelFile', 
                        help='the mnist label file')
    parser.add_argument('-oi', '--outputImageFile', 
                        help='the mnist image file to output')
    parser.add_argument('-ol', '--outputLabelFile', 
                        help='the mnist label file to output')
    parser.add_argument('-n', '--number', type=int,
                        help='the number of digits to save')
    args = parser.parse_args()

    N, rows, columns, digits, labels = load_mnist(args.imageFile, args.labelFile)

    print('count:', N, 'rows:', rows, 'columns:', columns)

    write_partial_mnist_data(digits, labels, args.number, args.outputImageFile, args.outputLabelFile)
