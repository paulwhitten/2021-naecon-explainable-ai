import pickle
import json
import numpy as np
from sklearn.neural_network import MLPClassifier
import pickle


with open('all.training.json') as infile:
    allData = json.load(infile)

count = 0
input_data = []
for preds in allData['predictions']:
    #if count == 0:
        #print(preds)
        #print(numpy.concatenate(preds))
    flat_list = np.concatenate(preds)
    flat_floats = np.zeros(len(flat_list), np.float)
    for i in range(len(flat_list)):
        if flat_list[i] != 0:
            flat_floats[i] = float(flat_list[i])
    input_data.append(flat_floats)
    #print(flat_floats)
    count = count + 1

output_data = []
for label in allData['labels']:
    row = np.zeros(10, np.float)
    row[label] = 1.0
    output_data.append(row)
    #print(row)

print(count)

hidden_layers = (100, 200)

print('hidden layers:', hidden_layers)

# original
mlp = MLPClassifier(hidden_layer_sizes=hidden_layers, activation='relu', max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp.fit(input_data, output_data)
print("Training set score: %f" % mlp.score(input_data, output_data))

pickle.dump(mlp, open('mnist_properties.model', 'wb'))



with open('all.test.json') as in_test_file:
    all_test_data = json.load(in_test_file)

count = 0
input_test_data = []
ex_exists_for_class = 0
in_ex_exists_for_class = 0
correct_classification = 0
incorrect_classification = 0
for preds in all_test_data['predictions']:
    flat_list = np.concatenate(preds)
    flat_floats = np.zeros(len(flat_list), np.float)
    for i in range(len(flat_list)):
        if flat_list[i] != 0:
            flat_floats[i] = float(flat_list[i])
    # get the particular prediction for this test item
    this_pred = mlp.predict([flat_floats])
    print(preds, 'pred:', this_pred, 'label:', all_test_data['labels'][count])
    #we want the numerical predication to compare against the label
    this_selection = -1
    p_count = 0
    for p in this_pred[0]:
        if p == 1:
            if this_selection == -1:
                this_selection = p_count
            else: # multiple predictions so bail
                this_selection = -1
                break
        p_count += 1
    if this_selection == all_test_data['labels'][count]:
        correct_classification += 1
        found = False
        for prop in preds:
            pred_id = 0
            for p in prop:
                if p == 1:
                    if pred_id == this_selection:
                        found = True
                        break
                pred_id += 1
        if found:
            ex_exists_for_class += 1
            print('explainable')
        else:
            print('not explainable')
    else:
        incorrect_classification += 1
        found = False
        for prop in preds:
            pred_id = 0
            for p in prop:
                if p == 1:
                    if pred_id == this_selection:
                        found = True
                        break
                pred_id += 1
        if found:
            in_ex_exists_for_class += 1

    input_test_data.append(flat_floats)
    #print(flat_floats)
    count = count + 1

output_test_data = []
for label in all_test_data['labels']:
    row = np.zeros(10, np.float)
    row[label] = 1.0
    output_test_data.append(row)
    #print(row)

print(count)

print("Test set score: %f" % mlp.score(input_test_data, output_test_data))
print('correct:', correct_classification, 'explainable:', ex_exists_for_class)
print('incorrect:', incorrect_classification, 'explainable:', in_ex_exists_for_class)

pickle.dump(mlp, open('mnist_nn_vote.model', 'wb'))
