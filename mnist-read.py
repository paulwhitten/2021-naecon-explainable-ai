import struct # https://docs.python.org/3/library/struct.html#struct.unpack
import numpy as np
from skimage import io

data = open("~/mnist/download/train-images-idx3-ubyte", "rb").read()

# initial position to read to for header
position = 16

# read big endian header
(magic, N, rows, columns) = struct.unpack(">iiii", data[:position])

print("Magic number:", hex(magic))
print("images:", N, "rows:", rows, "columns:", columns)

pixelsInImage =  rows * columns

imageCount = 0
for i in range(N):
    # read a byte buffer for the image
    pixels = struct.unpack("B" * pixelsInImage, data[position: position + pixelsInImage])
    position += rows * columns # advance the position
    img = np.zeros([rows,columns], np.uint8)
    pixel = 0
    for y in range(rows):
        for x in range(columns):
            #img[y,x] = 255 if pixels[pixel] > 75 else 0
            img[y,x] = pixels[pixel]
            pixel += 1
    imageCount += 1
    io.imsave("../data-py/img/data" + str(i) + ".png", img)

print("read", imageCount, "images")