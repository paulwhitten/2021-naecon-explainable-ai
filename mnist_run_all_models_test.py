from load_mnist_data import load_mnist_float
from load_mnist_data import load_mnist
import pickle
import json
import argparse

parser = argparse.ArgumentParser(description='Trains the models')
parser.add_argument('-m', '--mnist_folder', 
                        help='The mnist folder')
args = parser.parse_args()

'''
Executes all models saving results in a json file
'''

property_descriptions = ['skeleton', 'circle', 'crossing', 'ellipse', 'ellipseCircle', 'endpoint', 'fill', 'line', 'skeletonFill']

model_files = ['./models/mnist_skeleton.model', './models/mnist_circle.model', './models/mnist_crossing.model', './models/mnist_ellipse.model', './models/mnist_ellipse_circle.model',
  './models/mnist_endpoint.model', './models/mnist_fill.model', './models/mnist_line.model', './models/mnist_skel_fill.model']

image_files = ['./test_transforms/skel-image', './test_transforms/circle-image', './test_transforms/crossing-image',
  './test_transforms/ellipse-image', './test_transforms/ellipse_circle-image', './test_transforms/endpoint-image',
  './test_transforms/fill-image', './test_transforms/line-image', './test_transforms/skel-fill-image']

label_files = [args.mnist_folder + '/t10k-labels-idx1-ubyte', args.mnist_folder + '/t10k-labels-idx1-ubyte', args.mnist_folder + '/t10k-labels-idx1-ubyte', args.mnist_folder + '/t10k-labels-idx1-ubyte',
  args.mnist_folder + '/t10k-labels-idx1-ubyte', args.mnist_folder + '/t10k-labels-idx1-ubyte', args.mnist_folder + '/t10k-labels-idx1-ubyte',
  args.mnist_folder + '/t10k-labels-idx1-ubyte', args.mnist_folder + '/t10k-labels-idx1-ubyte']

n, rows, cols, raw_digits, raw_labels = load_mnist(args.mnist_folder + '/t10k-images-idx3-ubyte', args.mnist_folder + '/t10k-labels-idx1-ubyte')
int_labels = []
for label in raw_labels:
    int_labels.append(int(label))


print("loaded raw")

models = []
images = []
labels = []
scores = []

for i in range(len(model_files)):
    models.append(pickle.load(open(model_files[i], 'rb')))
    i_n, i_rows, i_columns, i_digits, i_labels = load_mnist_float(image_files[i], label_files[i])
    images.append(i_digits)
    labels.append(i_labels)
    scores.append(models[i].score(i_digits, i_labels))

print("loaded models and images")

property_results = []
proba_results = []
for i in range(len(images[0])):
    property_predictions = []
    probas = []
    for j in range(len(model_files)):
        prediction = models[j].predict([images[j][i]])
        proba = models[j].predict_proba([images[j][i]])
        predictions = []
        for p in prediction[0]:
            predictions.append(int(p))
        property_predictions.append(predictions)
        probas.append(proba[0].tolist())
    property_results.append(property_predictions)
    proba_results.append(probas)

output_json = {}

output_json['predictions'] = property_results
output_json['probabilitiyPredictions'] = proba_results
output_json['labels'] = int_labels
output_json['propertyDescriptions'] = property_descriptions
output_json['propertyScores'] = scores

with open('all.test.json', 'w') as outfile:
    json.dump(output_json, outfile)