import struct # https://docs.python.org/3/library/struct.html#struct.unpack
import numpy as np

# loads the mnist image and label file for processing in ml
# output is:
#   the number digits
#   the number of rows per digit
#   the number of columns per digit
#   an array of normalized pixel data
#   an array of floating point label data
def load_mnist_float(image_file, label_file):
    image_data = open(image_file, "rb").read()
    image_labels = open(label_file, "rb").read()

    # initial position to read to for header
    image_position = 16
    label_position = 8

    # read big endian header
    (image_magic, N, rows, columns) = struct.unpack(">iiii", image_data[:image_position])
    (label_magic, num_labels) = struct.unpack(">ii", image_labels[:label_position])
    if (N != num_labels):
        print("number of labels does not correspond to digits")

    pixels_in_image =  rows * columns

    digits = []
    labels = []
    image_count = 0
    while image_count < N:
        # read a byte buffer for the label and then the image
        label = struct.unpack("B", image_labels[label_position:label_position+1])
        pixels = struct.unpack("B" * pixels_in_image, image_data[image_position: image_position + pixels_in_image])
        norm_pixels = []
        # advance the position
        image_position += rows * columns
        label_position += 1
        for pixel in pixels:
            norm_pixels.append(pixel/255)
        digits.append(norm_pixels)
        output = np.zeros(10)
        output[label[0]] = 1.0
        labels.append(output)
        image_count += 1
    return (N, rows, columns, digits, labels)

# loads the mnist image and label file 
# output is:
#   The number of digits
#   the number of rows per digit
#   the number of columns per digit
#   an int array of digit pixel data
#   an array of integer labels
def load_mnist(image_file, label_file):
    image_data = open(image_file, "rb").read()
    image_labels = open(label_file, "rb").read()

    # initial position to read to for header
    image_position = 16
    label_position = 8

    # read big endian header
    (image_magic, N, rows, columns) = struct.unpack(">iiii", image_data[:image_position])
    print('image magic num:', hex(image_magic), 'N:', N, 'rows:', rows, 'columns:', columns)
    (label_magic, num_labels) = struct.unpack(">ii", image_labels[:label_position])
    print('label magic num:', hex(label_magic), 'num_labels:', num_labels)
    if (N != num_labels):
        print("number of labels does not correspond to digits")

    pixels_in_image =  rows * columns

    digits = []
    labels = []
    image_count = 0
    while image_count < N:
        # read a byte buffer for the label and then the image
        label = struct.unpack("B", image_labels[label_position:label_position+1])
        pixels = struct.unpack("B" * pixels_in_image, image_data[image_position: image_position + pixels_in_image])
        # advance the position
        image_position += rows * columns
        label_position += 1
        digits.append(pixels)
        labels.append(label[0])
        image_count += 1
    return (N, rows, columns, digits, labels)
