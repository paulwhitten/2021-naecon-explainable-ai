import numpy as np
from load_mnist_data import load_mnist_float
from sklearn.neural_network import MLPClassifier

# mlp hidden layers
#hidden_layers = (50,)
hidden_layers = (400,800)

home_folder = "/Users/pcw"
print("hidden layers:", hidden_layers)

# load training skeleton to calculate the weight
N, rows, columns, skel_digits, skel_labels = load_mnist_float(
    home_folder + "/mnist/train-transform/skel-image", home_folder + "/mnist/train-transform/skel-labels")

digit_weights = []
for digit in skel_digits:
    weight = 0.0
    for pixel in digit:
        if (pixel > 100):
            weight += 1.0
    weights = []
    for i in range(10):
        weights.append(weight / 784.0)
    digit_weights.append(weights)
# done calculating weight

# endpoints and skel fill fill
print('')
print('')
print("======Loading skel, endpoints, skel fill, line, and ellipse training data.======")
N, rows, columns, training_digits, training_labels = load_mnist_float(
    home_folder + "/mnist/train-transform/skel-fill-image", home_folder + "/mnist/train-transform/skel-fill-labels")

Ni, rowsi, columnsi, training_digitsi, training_labelsi = load_mnist_float(
    home_folder + "/mnist/train-transform/endpoint-image", home_folder + "/mnist/train-transform/endpoint-labels")

Nj, rowsj, columnsj, training_digitsj, training_labelsj = load_mnist_float(
    home_folder + "/mnist/train-transform/line-image", home_folder + "/mnist/train-transform/line-labels")

Nk, rowsjk, columnsk, training_digitsk, training_labelsk = load_mnist_float(
    home_folder + "/mnist/train-transform/ellipse-image", home_folder + "/mnist/train-transform/ellipse-labels")

print("loaded", Ni)

train_combined = []

for i in range(Ni):
    train_combined.append(np.concatenate((skel_digits[i], training_digits[i], training_digitsi[i], training_digitsj[i], training_digitsk[i])))

mlp = MLPClassifier(hidden_layer_sizes=hidden_layers, max_iter=500, alpha=1e-4,
                    solver='sgd', verbose=10, random_state=1,
                    learning_rate_init=.1)

mlp.fit(train_combined, training_labels)
print("Training set score: %f" % mlp.score(train_combined, training_labels))

n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/skel-fill-image", home_folder + "/mnist/test-transform/skel-fill-labels")

print("Loading endpoint test data.")
ni, rowsi, columnsi, test_digitsi, test_labelsi = load_mnist_float(
    home_folder + "/mnist/test-transform/endpoint-image", home_folder + "/mnist/test-transform/endpoint-labels")
print("loaded endpoint", ni)

print("Loading line test data.")
nj, rowsj, columnsj, test_digitsj, test_labelsj = load_mnist_float(
    home_folder + "/mnist/test-transform/line-image", home_folder + "/mnist/test-transform/line-labels")
print("loaded line test data", nj)

print("Loading ellipse test data.")
nk, rowsk, columnsk, test_digitsk, test_labelsk = load_mnist_float(
    home_folder + "/mnist/test-transform/ellipse-image", home_folder + "/mnist/test-transform/ellipse-labels")
print("loaded ellipse test data", nj)

# load the test skelton to calculate the weight
n, rows, columns, orig_skel_digits, orig_test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/skel-image", home_folder + "/mnist/test-transform/skel-labels")

test_weights = []
for digit in orig_skel_digits:
    weight = 0.0
    for pixel in digit:
        if (pixel > 100):
            weight += 1.0
    weights = []
    for i in range(10):
        weights.append(weight / 784.0)
    test_weights.append(weights)
# done calculating the weight 

combined_test = []
for i in range(ni):
    combined_test.append(np.concatenate((orig_skel_digits[i], test_digits[i], test_digitsi[i], test_digitsj[i], test_digitsk[i])))

print("Test set score: %f" % mlp.score(combined_test, test_labels))