import argparse
from load_mnist_data import load_mnist_float
from sklearn.neural_network import MLPClassifier
import pickle
import matplotlib.pyplot as plt
import numpy as np

parser = argparse.ArgumentParser(description='loads a model, data, and labels then does prediction.')
parser.add_argument('-m', '--modelFile', 
                    help='The pickled model')
parser.add_argument('-i', '--imageFile', 
                    help='The mnist image input file')
parser.add_argument('-l', '--labelFile', 
                    help='The mnist label input file')
parser.add_argument('-t', '--title', 
                    help='The title of the plot')
parser.add_argument('-o', '--outputFolder', 
                    help='The folder to output')
args = parser.parse_args()

mlp = pickle.load(open(args.modelFile, 'rb'))
print("loaded model from", args.modelFile)
n, rows, columns, digits, labels = load_mnist_float(args.imageFile, args.labelFile)
print("loaded", n, "from", args.imageFile, "and", args.labelFile)
score = mlp.score(digits, labels)
print("Model score: %f" % score)


frequency = [0] * 11
incorrect_frequency = [0] * 10
incorrect_count = 0
i = 0
for digit in digits:
    """
    print("label:", labels[i], "prediction:", mlp.predict([digit]))
    print("label:", labels[i], "pred proba:", mlp.predict_proba([digit]))
    if (i > 25):
        break
    """
    prediction = mlp.predict([digit])
    j = 0
    found = 10
    for p in prediction[0]:
        if (p != 0):
            if (found == 10):
                found = j
            else:
                found = 10
                break
        j = j + 1
    if (found < 10 and labels[i][found] == 0):
        found = 10
    frequency[found] = frequency[found] + 1
    if (found == 10):
        incorrect_count = incorrect_count + 1
        label_no = 0
        for label in labels[i]:
            if (label != 0):
                incorrect_frequency[label_no] = incorrect_frequency[label_no] + 1
            label_no = label_no + 1
    i = i + 1

values = []
for count in frequency:
    values.append(count / i)

'''
plt.hist(frequency, density=True, bins=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
plt.show()
'''

score_str = "{:.3f}".format(score)

x = np.arange(11)
fig, ax = plt.subplots()
plt.ylim(0.0, 1.0)
rects = ax.bar(x, height=values)
plt.xticks(x, ['0','1','2','3','4','5','6','7','8','9', 'wrong'])
ax.set_title(args.title + " property classifications, score: " + score_str)
for rect in rects:
    height = rect.get_height()
    ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%.2f' % height,
                ha='center', va='bottom')
plt.savefig(args.outputFolder + "/" + args.title + "-frequency.png")
#plt.show()

plt.clf()

if (incorrect_count > 0):
    incorrect_values = []
    for count in incorrect_frequency:
        incorrect_values.append(count / incorrect_count)

    y = np.arange(10)
    fig, ax = plt.subplots()
    plt.ylim(0.0, 1.0)
    rects = ax.bar(y, height=incorrect_values)
    plt.xticks(y, ['0','1','2','3','4','5','6','7','8','9'])
    plt.title(args.title + " property incorrect per digit")
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%.2f' % height,
                ha='center', va='bottom')
    plt.savefig(args.outputFolder + "/" + args.title + "-incorrect-frequency.png")
    #plt.show()