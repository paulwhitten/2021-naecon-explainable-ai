# method

Prerequisites are:

- A download of the binary MNIST distribution
- python3
- node.js
- going through set-up below

The following steps are used to attain results from the work:

1. Run `python mnist_transform.py` with appropriate parameters to do the
   transforms on training and test data. For example:
   ```bash
   python mnist_transform.py -i ~/mnist/downloads/train-images-idx3-ubyte -l ~/mnist/downloads/train-labels-idx1-ubyte -o ./training_transforms
   python mnist_transform.py -i ~/mnist/downloads/t10k-images-idx3-ubyte -l ~/mnist/downloads/t10k-labels-idx1-ubyte -o ./test_transforms
   ```
   This is a pretty slow single threaded script that does several transforms
   on the images. Better (more efficient) versions of this script were later
   developed.
1. run `python mnist_train_multiple.py` to train and save all property nn models in the `./models` folder.  TODO: may need to tweak paths in this script
1. run `python mnist_run_all_models.py` to produce `all.json`. This json file represents reprocessing the training set.  TODO: may need to tweak paths in this script.
1. run `python mnist_run_all_models_test.py` to produce `all.test.json`. TODO: may need to tweak paths in this script.
1. run `node process-predictions.js` to produce `all-with-names.json`
1. run `node process-predictions.js all.test.json` to produce `all-with-names.test.json`
1. run `node read-all.js` to produce `knowledgebase.json` and `predictionFrequencies.json`
1. run `node read-all.js all-with-names.test.json` to produce `knowledgebase.test.json`
1. run `node process-knowledgebase.json knowledgebase.test.json` to produce `knowledgebase-pred.json` which will contain each image and prediction probabilities for the test set.

## set-up

Configure a python virtual environment so you only impact the local virtual environment.

```bash
python3 -m venv ./.venv
```

Activate the virtual environment

```bash
source ./.venv/bin/activate
```

Install dependencies saved in `requirements.txt` like numpy, skimage and sklearn.

```bash
pip install -r requirements.txt
```

Download and extract the mnist binary distribution.
remember the location as it will be needed.
Examples on this page assume mnist is in `~/mnist/downloads`.

### training results

```
% node process-knowledgebase.js
count: 60000
correct: 58396 count: 60000 prediction accuracy: 0.9732666666666666
incorrects per digit: [
    2,  0, 205, 293, 283,
  146, 68, 475,  30, 102
]
```

## test results for probability voting

```
 % node process-knowlegebase.js 
count: 10000
correct: 9188 count: 10000 prediction accuracy: 0.9188
incorrects per digit: [
   10,  3, 121, 89, 117, 102, 52, 209, 53,  56
]
```

2023-05-31
```
% node process-knowledgebase.js knowledgebase.test.json 
count: 10000
correct: 9237 count: 10000 prediction accuracy: 0.9237
incorrects per digit: [
  10,  3, 109, 103, 115,
  96, 49, 173,  49,  56
]
```

2023-05-31 With chull
```
$ node process-knowledgebase.js knowledgebase.test.json 
count: 10000
correct: 9515 count: 10000 prediction accuracy: 0.9515
incorrects per digit: [
  11,  2, 58, 73, 58,
  71, 29, 80, 42, 61
]
```

## method for steps on nn voting system

* run steps 1-3 above on training data to produce all.json
* run `python mnist_nn_vote` to train the voting NN based on the results of property nn training data and test the test data


### test results for nn voting

```
% python mnist_nn_vote.py
60000
hidden layers: (100, 200)
Iteration 1, loss = 0.16671173
Iteration 2, loss = 0.00125578
Iteration 3, loss = 0.00070880
Iteration 4, loss = 0.00050568
Iteration 5, loss = 0.00040216
Iteration 6, loss = 0.00034219
Iteration 7, loss = 0.00030118
Iteration 8, loss = 0.00027112
Iteration 9, loss = 0.00025004
Iteration 10, loss = 0.00023341
Iteration 11, loss = 0.00022007
Iteration 12, loss = 0.00020949
Iteration 13, loss = 0.00020055
Iteration 14, loss = 0.00019320
Iteration 15, loss = 0.00018693
Iteration 16, loss = 0.00018156
Training loss did not improve more than tol=0.000100 for 10 consecutive epochs. Stopping.
Training set score: 1.000000
10000
Test set score: 0.959300
```