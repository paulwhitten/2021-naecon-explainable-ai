from skimage.transform import probabilistic_hough_line
import matplotlib.pyplot as plt
from skimage.draw import line
from matplotlib import cm
from skimage import io
import numpy as np

# Line finding using the Probabilistic Hough Transform
image = io.imread("../data-py/skel-lee/2-1.png")
lines = probabilistic_hough_line(image, threshold=0, line_length=5,
                                 line_gap=0)

new_image = np.zeros((image.shape[0], image.shape[1]), np.uint8)

# Generating figure 2
fig, axes = plt.subplots(1, 2, figsize=(15, 5), sharex=True, sharey=True)
ax = axes.ravel()

ax[0].imshow(image, cmap=cm.gray)
ax[0].set_title('Input image')

for l in lines:
    print("line:", l)
    p0, p1 = l
    ax[1].plot((p0[0], p1[0]), (p0[1], p1[1]))
    rr, cc = line(l[0][0], l[0][1], l[1][0], l[1][1])
    new_image[cc,rr] = 255
ax[1].set_xlim((0, image.shape[1]))
ax[1].set_ylim((image.shape[0], 0))
ax[1].set_title('Probabilistic Hough')

io.imsave("./test.png", new_image)

for a in ax:
    a.set_axis_off()

plt.tight_layout()
plt.show()