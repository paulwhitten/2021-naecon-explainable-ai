from load_mnist_data import load_mnist_float
from sklearn.neural_network import MLPClassifier
import pickle

# mlp hidden layers
#hidden_layers = (50,)
hidden_layers = (100, 200)

print('hidden layers:', hidden_layers)

home_folder = "/Users/pcw"

# original

print("Loading test data.")
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/download/t10k-images-idx3-ubyte", home_folder + "/mnist/download/t10k-labels-idx1-ubyte")
print("loaded", n)
print('')
print('')
print("======loading raw model======")
mlp = pickle.load(open('mnist_raw.model', 'rb'))
print("Test set score: %f" % mlp.score(test_digits, test_labels))


# skeleton
print('')
print('')
print("======Loading skeleton model======")
mlp1 = pickle.load(open('mnist_skeleton.model', 'rb'))
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/skel-image", home_folder + "/mnist/test-transform/skel-labels")
print("loaded", n)
print("Test set score: %f" % mlp1.score(test_digits, test_labels))


# endpoints
print('')
print('')
print("======Loading endpoint model======")
mlp2 = pickle.load(open('mnist_endpoint.model', 'rb'))
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/endpoint-image", home_folder + "/mnist/test-transform/endpoint-labels")
print("loaded", n)
print("Test set score: %f" % mlp2.score(test_digits, test_labels))


# crossings
print('')
print('')
print("======Loading crossing model======")
mlp3 = pickle.load(open('mnist_crossing.model', 'rb'))
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/crossing-image", home_folder + "/mnist/test-transform/crossing-labels")
print("loaded", n)
print("Test set score: %f" % mlp3.score(test_digits, test_labels))


# fill
print('')
print('')
print("======Loading fill model.======")
mlp4 = pickle.load(open('mnist_fill.model', 'rb'))
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/fill-image", home_folder + "/mnist/test-transform/fill-labels")
print("loaded", n)
print("Test set score: %f" % mlp4.score(test_digits, test_labels))


# skel fill
print('')
print('')
print("======Loading skel fill model======")
mlp5 = pickle.load(open('mnist_skel_fill.model', 'rb'))
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/skel-fill-image", home_folder + "/mnist/test-transform/skel-fill-labels")
print("loaded", n)
print("Test set score: %f" % mlp5.score(test_digits, test_labels))


# lines
print('')
print('')
print("======Loading line model======")
mlp6 = pickle.load(open('mnist_line.model', 'rb'))
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/line-image", home_folder + "/mnist/test-transform/line-labels")
print("loaded", n)
print("Test set score: %f" % mlp6.score(test_digits, test_labels))


# ellipse
print('')
print('')
print("======Loading ellipse model======")
mlp7 = pickle.load(open('mnist_ellipse.model', 'rb'))
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/ellipse-image", home_folder + "/mnist/test-transform/ellipse-labels")
print("loaded", n)
print("Test set score: %f" % mlp7.score(test_digits, test_labels))


# circle
print('')
print('')
print("======Loading circle model======")
mlp8 = pickle.load(open('mnist_circle.model', 'rb'))
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/circle-image", home_folder + "/mnist/test-transform/circle-labels")
print("loaded", n)
print("Test set score: %f" % mlp8.score(test_digits, test_labels))


# ellipse or circle
print('')
print('')
print("======Loading ellipse circle model======")
mlp9 = pickle.load(open('mnist_ellipse_circle.model', 'rb'))
n, rows, columns, test_digits, test_labels = load_mnist_float(
    home_folder + "/mnist/test-transform/ellipse_circle-image", home_folder + "/mnist/test-transform/ellipse_circle-labels")
print("loaded", n)
print("Test set score: %f" % mlp9.score(test_digits, test_labels))
